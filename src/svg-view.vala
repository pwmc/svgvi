/* svg-view.vala
 *
 * Copyright 2019 Daniel Espinosa Ortiz <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

public class Svgvi.SvgView : Gtk.Box {
  private GSvgtk.DrawingArea _image;
  private GSvgtk.DrawingArea loading;
  private Gtk.ScrolledWindow sw;
  private GSvg.Document _svg;
  private GSvg.Document _viewer;
  private GSvg.DomSvgElement _view;
  private GSvg.DomSvgElement _current;
  private GSvg.DomSvgElement _top_rule;
  private GSvg.DomSvgElement _side_rule;
  private string _rules_style;
  private GSvg.DomRectElement rectv;
  private GSvg.DomRectElement recth;
  private Gtk.Stack stack;
  private double size = 0.1;
  private bool loading_status = false;

  public GSvg.Document svg {
    get { return _svg; }
    set {
      _svg = value;
      assign_svg ();
    }
  }
  public string rules_style {
    get {
      return _rules_style;
    }
    set {
      _rules_style = value;
      generate_view ();
      assign_svg ();
    }
  }
  construct {
    sw = new Gtk.ScrolledWindow ();
    stack = new Gtk.Stack ();
    sw.set_child (stack);
    append (sw);
    _image = new GSvgtk.DrawingAreaGtk ();
    _image.width_mm = -1;
    _image.height_mm = -1;
    ((Gtk.DrawingArea) _image).content_width = 1200;
    ((Gtk.DrawingArea) _image).content_height = 2000;
    _image.renderer.width = 600;
    _image.renderer.height = 600;
    _rules_style = "fill: gray";
    var f = File.new_for_uri ("resource:///mx/pwmc/Svgvi/logo-background.svg");
    generate_view ();
    try {
      var ostream = new MemoryOutputStream.resizable ();
      var istream = f.read ();
      var dstream = new GLib.DataInputStream (istream);
      size_t s = 0;
      string str = dstream.read_upto ("\0", -1, out s, null);
      _current = _view.add_svg ("5mm", "5mm", null, null);
      var isvg = _current.create_svg (null, null, null, null, null, null, null);
      isvg.read_from_string (str);
      _current.append_child (isvg);
      var nsvg = new GSvg.Document ();
      nsvg.read_from_string (str);
      _svg = nsvg;
      _image.invalidate ();
    } catch (GLib.Error e) {
      warning ("Error loading default background: %s", e.message);
    }

    var sw2 = new Gtk.ScrolledWindow ();
    var pv = new Gtk.Viewport (null, null);
    pv.set_child (_image as Gtk.Widget);
    sw2.set_child (pv);
    sw2.hexpand = true;
    sw2.vexpand = true;
    stack.add_named (sw2, "image");
    stack.set_visible_child_name ("image");
    var f2 = File.new_for_uri ("resource:///mx/pwmc/Svgvi/loading.svg");
    try {
      var ostream2 = new MemoryOutputStream.resizable ();
      var istream2 = f2.read ();
      ostream2.splice (istream2, OutputStreamSpliceFlags.CLOSE_SOURCE, null);
      var nsvg2 = new GSvg.Document ();
      nsvg2.read_from_string ((string) ostream2.get_data ());
      loading = new GSvgtk.DrawingAreaGtk ();
      stack.add_named (loading as Gtk.Widget, "loading");
      loading.svg = nsvg2;
      loading.invalidate ();
    } catch (GLib.Error e) {
      warning ("Error loading default loading image: %s", e.message);
    }
    sw.hexpand = true;
    sw.vexpand = true;
    // sw.scroll_child.connect ((scroll, hor)=>{
    //   switch (scroll) {
    //     case NONE:
    //     break;
    //     case JUMP:
    //     case STEP_BACKWARD:
    //     break;
    //     case STEP_FORWARD:
    //     break;
    //     case PAGE_BACKWARD:
    //     case PAGE_FORWARD:
    //     case STEP_UP:
    //     case STEP_DOWN:
    //     case PAGE_UP:
    //     case PAGE_DOWN:
    //     case STEP_LEFT:
    //     case STEP_RIGHT:
    //     case PAGE_LEFT:
    //     case PAGE_RIGHT:
    //     case START:
    //     case END:
    //     break;
    //   }
    // });
  }

  private void assign_svg () {
    try {
      loading_status = true;
      GLib.Timeout.add (1000, ()=>{
        if(!loading_status) {
          return Source.REMOVE;
        }
        stack.set_visible_child_name ("loading");
        GLib.Timeout.add (500, ()=>{
          size = size * 1.1;
          if (size > 3.0) {
            size = 0.1;
          }
          var g = loading.svg.root_element.groups.get ("main");
          if (g != null) {
            g.transform.value = "scale(%s)".printf (GSvg.DomPoint.double_to_string (size));
            loading.invalidate ();
          }
          if (!loading_status) {
            return Source.REMOVE;
          }
          return Source.CONTINUE;
        });
        return Source.REMOVE;
      });
      if (_current != null) {
        _current.remove ();
        _current = null;
      }
      if (_svg != null) {
        _current = _view.add_svg ("5mm", "5mm", null, null);
        var nsvg = _current.add_svg (null, null, null, null);
        nsvg.read_from_string (_svg.write_string ());
        _current.append_child (nsvg);
      }
      new Thread<int> ("rendering", ()=>{
        loading_status = true;
        _image.invalidate ();
        loading_status = false;
        if (stack.get_visible_child_name () != "image") {
          message ("Changed to image");
          stack.set_visible_child_name ("image");
        }
        return 0;
      });
    } catch (GLib.Error e) {
      warning ("Error parsing SVG source: %s", e.message);
    }
  }
  private void generate_view () {
    try {
      _viewer = new GSvg.Document ();
      _view = _viewer.root_element;
      _view.set_attribute ("width", "225.9mm");
      _view.set_attribute ("height", "289.4mm");
      _top_rule = _view.add_svg ("0mm", "0mm", "225.9mm", "289.4mm");
      recth = _top_rule.create_rect ("5mm","0mm","215.9mm","5mm", null, null);
      _top_rule.append_child (recth);
      recth.style = new GSvg.CssStyleDeclaration ();
      recth.style.css_text = rules_style;
      var lstyle = new GSvg.CssStyleDeclaration ();
      lstyle.css_text = """stroke: black; stroke-width: 0.25mm""";
      var lh = _view.create_line ("5mm","0mm","5mm","5mm");
      _top_rule.append_child (lh);
      lh.style = lstyle;
      var lx1 = lh.x1.base_val.value;
      var utlx1 = lh.x1.base_val.unit_type;
      var lx2 = lh.x2.base_val.value;
      var utlx2 = lh.x2.base_val.unit_type;
      var ly1 = lh.y1.base_val.value;
      var utly1 = lh.y1.base_val.unit_type;
      var ly2 = lh.y2.base_val.value;
      var utly2 = lh.y2.base_val.unit_type;
      bool middle = true;
      int n = 0;
      while (lx1 + 5 < recth.width.base_val.value) {
        lx1 += 5;
        lx2 += 5;
        var nlh = Object.new (typeof(GSvg.LineElement), "owner_document", _viewer) as GSvg.DomLineElement;
        nlh.style = lstyle;
        nlh.x1 = new GSvg.AnimatedLengthX () as GSvg.DomAnimatedLengthX;
        nlh.x1.base_val = new GSvg.Length () as GSvg.DomLength;
        nlh.x1.base_val.value = lx1;
        nlh.x1.base_val.unit_type = utlx1;
        nlh.x2 = new GSvg.AnimatedLengthX () as GSvg.DomAnimatedLengthX;
        nlh.x2.base_val.value = lx2;
        nlh.x2.base_val.unit_type = utlx2;
        nlh.y1 = new GSvg.AnimatedLengthY () as GSvg.DomAnimatedLengthY;
        nlh.y1.base_val.value = ly1;
        nlh.y1.base_val.unit_type = utly1;
        nlh.y2 = new GSvg.AnimatedLengthY () as GSvg.DomAnimatedLengthY;
        nlh.y2.base_val.value = ly2;
        nlh.y2.base_val.unit_type = utly2;
        if (middle) {
          nlh.y2.base_val.value /= 2;
          middle = false;
        } else {
          middle = true;
        }
        _top_rule.append_child (nlh);
        var pos = lx1 - 5;
        if (n > 0 && (n/2.0) != (int) (n / 2.0)) {
          var txpos = new GSvg.Length ();
          txpos.value = nlh.x2.base_val.value + 0.2;
          txpos.unit_type = nlh.x2.base_val.unit_type;
          var typos = new GSvg.Length ();
          typos.value = nlh.y2.base_val.value - 0.5;
          typos.unit_type = nlh.y2.base_val.unit_type;
          var t = _top_rule.create_text ("%d".printf ((int) pos),
                      txpos.to_string (),
                      typos.to_string (),
                      null, null,
                      """font-family: Verdana; font-size: 2.5mm; fill: black""");
          _top_rule.append_child (t);
        }
        n++;
      }
      _side_rule = _view.add_svg ("0mm", "0mm", "225.9mm", "289.4mm");
      rectv = _side_rule.create_rect ("0mm","5mm","5mm","279.4mm", null, null);
      _side_rule.append_child (rectv);
      rectv.style = new GSvg.CssStyleDeclaration ();
      rectv.style.css_text = rules_style;
      var lv = _side_rule.create_line ("0mm","5mm","5mm","5mm");
      _side_rule.append_child (lv);
      lv.style = lstyle;
      lx1 = lv.x1.base_val.value;
      utlx1 = lh.x1.base_val.unit_type;
      lx2 = lv.x2.base_val.value;
      utlx2 = lh.x2.base_val.unit_type;
      ly1 = lv.y1.base_val.value;
      utly1 = lh.y1.base_val.unit_type;
      ly2 = lv.y2.base_val.value;
      utly2 = lv.y2.base_val.unit_type;
      n = 0;
      while (ly1 + 5 < rectv.height.base_val.value) {
        ly1 += 5;
        ly2 += 5;
        var nlv = Object.new (typeof(GSvg.LineElement), "owner_document", _viewer) as GSvg.DomLineElement;
        nlv.style = lstyle;
        nlv.x1 = new GSvg.AnimatedLengthX () as GSvg.DomAnimatedLengthX;
        nlv.x1.base_val.value = lx1;
        nlv.x1.base_val.unit_type = utlx1;
        nlv.x2 = new GSvg.AnimatedLengthX () as GSvg.DomAnimatedLengthX;
        nlv.x2.base_val.value = lx2;
        nlv.x2.base_val.unit_type = utlx2;
        nlv.y1 = new GSvg.AnimatedLengthY () as GSvg.DomAnimatedLengthY;
        nlv.y1.base_val.value = ly1;
        nlv.y1.base_val.unit_type = utly1;
        nlv.y2 = new GSvg.AnimatedLengthY () as GSvg.DomAnimatedLengthY;
        nlv.y2.base_val.value = ly2;
        nlv.y2.base_val.unit_type = utly2;
        if (middle) {
          nlv.x2.base_val.value /= 2;
          middle = false;
        } else {
          middle = true;
        }
        _side_rule.append_child (nlv);
        var pos = ly1 - 5;
        if (n > 0 && (n/2.0) != (int) (n / 2.0)) {
          var txpos = new GSvg.Length ();
          txpos.value = nlv.x1.base_val.value + 0.5;
          txpos.unit_type = nlv.x1.base_val.unit_type;
          var typos = new GSvg.Length ();
          typos.value = nlv.y1.base_val.value - 0.5;
          typos.unit_type = nlv.y1.base_val.unit_type;
          var t = _side_rule.create_text ("%d".printf ((int) pos),
                      txpos.to_string (),
                      typos.to_string (),
                      null, null,
                      """font-family: Verdana; font-size: 2.5mm; fill: black""");
          _side_rule.append_child (t);
        }
        n++;
      }

      _image.replace_svg (_viewer);
      _image.invalidate ();
    } catch (GLib.Error e) {
      warning ("SVG Viewer Initialization Error: %s", e.message);
    }
  }
}
