/* window.vala
 *
 * Copyright 2019 Daniel Espinosa Ortiz <esodan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GSvg;

[GtkTemplate (ui = "/mx/pwmc/Svgvi/window.ui")]
public class Svgvi.Window : Gtk.ApplicationWindow {
  [GtkChild]
  private unowned Gtk.Box bxmain;
  [GtkChild]
  private unowned Gtk.Label title_label;
  [GtkChild]
  private unowned Gtk.Button bopen;
  [GtkChild]
  private unowned Gtk.Button bsaveas;
  [GtkChild]
  private unowned Gtk.Button bcircle;
  [GtkChild]
  private unowned Gtk.Button bellipse;
  [GtkChild]
  private unowned Gtk.Button btext;
  [GtkChild]
  private unowned Gtk.Button bline;
  [GtkChild]
  private unowned Gtk.Button brect;
  [GtkChild]
  private unowned Gtk.Button mbnew;
  [GtkChild]
  private unowned Gtk.Button mbabout;
  [GtkChild]
  private unowned Gtk.Popover ptoolbox;

  private Gtk.AboutDialog dabout;
  
  private Svgvi.Editor editor;

  public File file {
    get { return editor.file; }
    set {
      editor.file = value;
    }
  }

  public int current_row {
    get { return editor.current_row; }
    set { editor.current_row = value; }
  }

  public int current_column {
    get { return editor.current_column; }
    set { editor.current_column = value; }
  }

  construct {
    maximized = true;
    dabout = new Gtk.AboutDialog ();
    editor = new Svgvi.Editor ();
    bxmain.append (editor);
    editor.hexpand = true;
    editor.vexpand = true;
    bopen.clicked.connect (()=>{
      ptoolbox.hide ();
      var fs = new Gtk.FileChooserNative ("Open",
                                          this,
                                          Gtk.FileChooserAction.OPEN,
                                          "_Select",
                                          "_Cancel");
      var ff = new Gtk.FileFilter ();
      ff.add_mime_type ("image/svg+xml");
      fs.filter = ff;
      fs.response.connect ((res)=>{
          if (res == Gtk.ResponseType.ACCEPT) {
            file = fs.get_file ();
          }

          fs.destroy ();
      });
      fs.show ();
    });
    bsaveas.clicked.connect (()=>{
      ptoolbox.hide ();
      var fs = new Gtk.FileChooserNative ("Save as..",
                                          this,
                                          Gtk.FileChooserAction.SAVE,
                                          "_Save",
                                          "_Cancel");
      var ff = new Gtk.FileFilter ();
      ff.add_mime_type ("image/svg+xml");
      fs.filter = ff;
      fs.response.connect ((res)=>{
          if (res == Gtk.ResponseType.ACCEPT) {
            editor.save_to (fs.get_file ());
            title_label.label = editor.file.get_basename ();
          }

          fs.destroy ();
      });
      fs.show ();
    });
    editor.loading.connect (()=>{
      if (file != null) {
        title_label.label = "Loading... "+file.get_basename ();
      }
    });
    editor.loaded.connect (()=>{
      if (file != null) {
        title_label.label = file.get_basename ();
      }
    });
    const string[] authors = {"PWMC Services SAS de CV", "Daniel Espinosa Ortiz"};
    dabout.set_authors (authors);
    dabout.set_copyright ("2019 PWMC Services SAS de CV\n2019-2022 Daniel Espinosa Ortiz");
    dabout.license = "gpl-3.0";
    dabout.program_name = "Svgvi";
    dabout.version = Config.VERSION;
    dabout.website = "https://gitlab.com/gsvg/svgvi/-/wikis/home";
    try {
        var logo = File.new_for_uri ("resource:///mx/pwmc/Svgvi/logo.svg");
        var handle = new Rsvg.Handle.from_gfile_sync (logo, Rsvg.HandleFlags.FLAGS_NONE);
        var pixbuf = handle.get_pixbuf ();
        var paintable = Gdk.Texture.for_pixbuf (pixbuf);
        dabout.set_logo (paintable);
        dabout.version = Config.VERSION;
    } catch (GLib.Error e) {
        warning ("Fail loading app icon");
    }
    mbabout.clicked.connect (()=>{
        ptoolbox.hide ();
        dabout.show ();
    });
    mbnew.clicked.connect (()=>{
      ptoolbox.hide ();
      var fs = new Gtk.FileChooserNative ("Save as..",
                                          this,
                                          Gtk.FileChooserAction.SAVE,
                                          "_Save",
                                          "_Cancel");
      var ff = new Gtk.FileFilter ();
      ff.add_mime_type ("image/svg+xml");
      fs.filter = ff;
      fs.modal = true;
      fs.response.connect ((res)=>{
          if (res == Gtk.ResponseType.ACCEPT) {
            try {
              var d = new GSvg.Document ();
              d.read_from_string ("""<svg xmlns="http://www.w3.org/2000/svg" width="200mm" height="200mm">
      <!--Write your shapes here!-->
    </svg>""");
              message (d.write_string ());
              d.write_file (fs.get_file ());
              file = fs.get_file ();
            } catch (GLib.Error e) {
              warning ("Error creating a new file: %s", e.message);
            }
          }
          fs.destroy ();
      });

      fs.show ();
    });
    /* Build shapes at cursor*/
    bcircle.clicked.connect (()=>{
      editor.write_circle ();
    });
    bline.clicked.connect (()=>{
      editor.write_line ();
    });
    brect.clicked.connect (()=>{
      editor.write_rect ();
    });
    bellipse.clicked.connect (()=>{
      editor.write_ellipse ();
    });
    btext.clicked.connect (()=>{
      editor.write_text ();
    });
  }

  public Window (Gtk.Application app) {
    GLib.Object (application: app);
  }

  public void toggle_search () {
    editor.toggle_search ();
  }
}
